#pragma once
#include <Poco/Net/HTTPRequestHandler.h>

#define REGISTER_HANDLER(name) \
    class name: public Poco::Net::HTTPRequestHandler \
    { \
    private: \
    void handleRequest( \
        Poco::Net::HTTPServerRequest& request, \
        Poco::Net::HTTPServerResponse& response) override; \
    };

namespace handlers {

REGISTER_HANDLER(Isp)

}
