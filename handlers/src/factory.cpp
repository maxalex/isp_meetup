#include <handlers.hpp>
#include <handlers/error.hpp>
#include <handlers/factory.hpp>
#include <Poco/Net/HTTPServerRequest.h>

namespace handlers
{

HTTPRequestHandler* Factory::GetMethodHandlers(const std::string &uri) const {
    if (uri == "/isp") {
        return new Isp();
    }
    return new Error(Poco::Net::HTTPResponse::HTTP_INTERNAL_SERVER_ERROR, "Wrong endpoint " + uri);
}

HTTPRequestHandler* Factory::PutMethodHandlers(const std::string &/*uri*/) const {
    return new Error(Poco::Net::HTTPResponse::HTTP_INTERNAL_SERVER_ERROR);
}

HTTPRequestHandler* Factory::PostMethodHandlers(const std::string &/*uri*/) const {
    return new Error(Poco::Net::HTTPResponse::HTTP_INTERNAL_SERVER_ERROR);
}

HTTPRequestHandler* Factory::DeleteMethodHandlers(const std::string &/*uri*/) const {
    return nullptr;
}

Poco::Net::HTTPRequestHandler* Factory::createRequestHandler(
    const Poco::Net::HTTPServerRequest& request)
{
    using Poco::Net::HTTPRequest;

    const auto method = request.getMethod();
    const auto uri = request.getURI();
    if (method == HTTPRequest::HTTP_GET) {
        return GetMethodHandlers(uri);
    } else if (method == HTTPRequest::HTTP_POST) {
        return PostMethodHandlers(uri);
    } else if (method == HTTPRequest::HTTP_PUT) {
        return PutMethodHandlers(uri);
    } else if (method == HTTPRequest::HTTP_DELETE) {
        return DeleteMethodHandlers(uri);
    }
    return new Error(Poco::Net::HTTPResponse::HTTP_INTERNAL_SERVER_ERROR);
}

} // namespace handlers