#include <handlers.hpp>
#include <Poco/Net/HTTPServerResponse.h>

namespace handlers
{

    void Isp::handleRequest(
        Poco::Net::HTTPServerRequest& request,
        Poco::Net::HTTPServerResponse& response)
    {
        response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
        response.send() << "ISP System meetings service";
    }

} // namespace handlers
